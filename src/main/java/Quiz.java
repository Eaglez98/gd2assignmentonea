import java.util.ArrayList;
import java.util.Scanner;

/*
 * Quiz class (with main method) which asks the user
 * to answer one of 3 randomly-generated questions.
 * Input-validation is checked whether the answer is correct or not.
 *
 * Created by Adam Zieba
 * GD2a (CA1)
 */
public class Quiz
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);

        ArrayList<String> Questions = new ArrayList<String>();
        Questions.add("Q1) What is 10/2");
        Questions.add("Q2) What's the best Institute in Ireland??");
        Questions.add("Q3) 100 * 0 is equal to?");

        ArrayList<String> Answers = new ArrayList<String>();
        Answers.add("5");
        Answers.add("DKIT");
        Answers.add("0");

        int randomPic = (int)(Math.random() * 3); //This line has been used from the example in class
        System.out.println(Questions.get(randomPic));

        String answer = sc.nextLine();
        if(answer.equals(Answers.get(randomPic)))
        {
            System.out.println("True! Answer is correct");
        }
        else
        {
            System.out.println("False! Answer is incorrect");
        }

    }
}
