import java.util.Scanner;

/*///////////////////////////////////////////////////////////
 * This is the Main file which displays User-menu and runs
 * the entire project.
 * Created by Adam Zieba (GD2a)
 *///////////////////////////////////////////////////////////

public class Main
{
    //Establishes scanner
    private static Scanner sc = new Scanner(System.in);

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Main method //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void main(String[] args)
    {
        boolean quitMenu = false;
        portrayOptions();

        while(quitMenu == false)
        {
            int choice = sc.nextInt();
            sc.nextLine();

            switch(choice)
            {
                case 0:
                    System.out.println("Quit the main menu");
                    quitMenu = true;
                    break;

                case 1:
                    printInformation();
                    break;

                case 2:
                    addMatrices();
                    break;

                case 3:
                    indexOfMinAndMax();
                    break;

                case 4:
                    circularShiftRight();
                    break;

                case 5:
                    //Credits();
                    break;
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Methods //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void portrayOptions()
    {
        System.out.println("////////////////////////////////////////////////");
        System.out.println("///  Please choose one of the below options  ///");
        System.out.println("///  Option 0: Quit the Menu by pressing 0   ///");
        System.out.println("///  Option 1: Print Program Information     ///");
        System.out.println("///  Option 2: Add Matrices                  ///");
        System.out.println("///  Option 3: Find Min & Max Index          ///");
        System.out.println("///  Option 4: Circular-Shift movement       ///");
        System.out.println("////////////////////////////////////////////////");
    }

    public static void printInformation()
    {
        System.out.println("This is a program that runs few different functions including: ");
        System.out.println(" - Add Matrices (matrix 1) + (matrix 2) = (new matrix) in a 4x3 format");
        System.out.println(" - Index Of Min And Max returning a pair of values from the array");
        System.out.println(" - Circular Array behaviour (Shift all elements by n places)");
    }

    public static void addMatrices()
    {
        int firstMatrix[][] = { {1,2,3}, {2,3,4}, {3,4,5}, {4,5,6} };
        int secondMatrix[][] = { {4,5,6}, {6,5,4}, {3,2,1}, {7,7,7} };
        int sumMatrix[][] = new int[4][3];

        //Calculation
        for (int i = 0; i < firstMatrix.length; i++)
        {
            for (int z = 0; z < firstMatrix[i].length; z++)
            {
                sumMatrix[i][z] = firstMatrix[i][z] + secondMatrix[i][z];
            }
        }

        System.out.println("The sum Matrix after addition of two matrices");
        for (int i = 0; i < firstMatrix.length; i++)
        {
            for (int z = 0; z < firstMatrix[i].length; z++)
            {
                System.out.print(sumMatrix[i][z] + "\t");
            }
            System.out.println();
        }
    }

    public static void indexOfMinAndMax()
    {
        int numberArray[] = new int []{1,2,3,4,5,6,7,8};
        int minNum = numberArray[0];
        int z;
        for (int i = 0; i < numberArray.length; i++)
        {
            for( z = 0; z <numberArray[z]; z++)
            {
                if(minNum > numberArray[i])
                {
                    minNum = numberArray[i];
                }
            }
//            if(maxNum > numberArray[i])
//            {
//                minNum = numberArray[i];
//            }
        }
   //     System.out.println("Min value = " + minNum + " Index = " + numberArray[z]);
    }

    public static void circularShiftRight()
    {
        int shiftRightArr[] = new int []{1,2,3,4,5,6,7,8};

        System.out.print("Original Arrray: ");
        for(int o = 0; o < shiftRightArr.length; o++)
        {
            System.out.print(shiftRightArr[o] + " ");
        }

        //Rotation method
        System.out.println("\n Please input the number of shifts: ");
        int numOfShifts = sc.nextInt();
        sc.nextLine();
//
//        if(numOfShifts > 0)
//        {
            for(int i = 0; i < numOfShifts; i++)
            {
                int z, lastNum;
                lastNum = shiftRightArr[shiftRightArr.length-1];

                for(z = shiftRightArr.length - 1; z > 0; z--)
                {
                    shiftRightArr[z] = shiftRightArr[z-1];
                }
                shiftRightArr[0] = lastNum;
            }
            System.out.print("Array after rotation: ");
            for(int r = 0; r < shiftRightArr.length; r++)
            {
                System.out.print(shiftRightArr[r] + " ");
            }
//        }
//        else if(numOfShifts <= 0)
//        {
//            System.out.println("Please input a positive whole number: ");
//            sc.nextLine();
//        }
    }
}