public class Player
{
    public String name;
    public int health;
    public int hitpoints;

    public Player(String name, int health)
    {
        this.name = " ";
        this.health = 100;
    }

    public String getName() { return name; }
    public int getHealth() { return health; }

    //Methods
    public void loseHealth(int damage)
    {
        this.hitpoints -= damage;
        if(this.hitpoints <= 0)
        {
            System.out.println("Player died");
            //Reduce number of lives
        }
    }
    public void updatePlayersHealth()
    {
        //Method should print out player's health after each hit
        //or health restoration.
    }

    public int healthRemaining() {return this.hitpoints;}
}
